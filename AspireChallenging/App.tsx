import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {DebitCardScreen} from './src/screens/debit-card-screen/index';
import {SpendingLimitScreen} from './src/screens/spending-limit-screen/index';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {View} from 'react-native';
import {COLORS} from '@theme';
import {PayIcon, HomeIcon, PaymenstIcon, CreditIcon, AccountIcon} from '@image';

const RootNavigationStack = createNativeStackNavigator();

const Tabbar = createBottomTabNavigator();

function EmptyScreen() {
  return <View />;
}

function MyTabs() {
  return (
    <Tabbar.Navigator
      initialRouteName="Debit Card"
      screenOptions={{
        tabBarActiveTintColor: COLORS.PRIMARY,
        tabBarInactiveTintColor: 'gray',
        headerShown: false,
      }}>
      <Tabbar.Screen
        name="Home"
        component={EmptyScreen}
        options={{
          tabBarIcon: ({}) => (
            <View>
              <HomeIcon />
            </View>
          ),
        }}
      />
      <Tabbar.Screen
        name="Debit Card"
        component={DebitCardScreen}
        options={{
          tabBarIcon: ({}) => (
            <View>
              <PayIcon />
            </View>
          ),
        }}
      />
      <Tabbar.Screen
        name="Payments"
        component={EmptyScreen}
        options={{
          tabBarIcon: ({}) => (
            <View>
              <PaymenstIcon />
            </View>
          ),
        }}
      />
      <Tabbar.Screen
        name="Credit"
        component={EmptyScreen}
        options={{
          tabBarIcon: ({color}) => (
            <View>
              <CreditIcon fill={COLORS.PRIMARY} />
            </View>
          ),
        }}
      />
      <Tabbar.Screen
        name="Profile"
        component={EmptyScreen}
        options={{
          tabBarIcon: ({}) => (
            <View>
              <AccountIcon />
            </View>
          ),
        }}
      />
    </Tabbar.Navigator>
  );
}

function RootStackScreen() {
  return (
    <RootNavigationStack.Navigator initialRouteName="TabBar">
      <RootNavigationStack.Screen
        name="TabBar"
        component={MyTabs}
        options={{headerShown: false}}
      />
      <RootNavigationStack.Screen
        name="SpendingLimit"
        component={SpendingLimitScreen}
        options={{
          headerShown: false,
        }}
      />
    </RootNavigationStack.Navigator>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
};

export default App;
