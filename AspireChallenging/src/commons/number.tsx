import {Platform} from 'react-native';

export function numberToLocalString(number: number) {
  if (Platform.OS === 'ios') {
    return number.toLocaleString();
  } else {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
}
