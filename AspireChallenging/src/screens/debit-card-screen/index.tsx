/* eslint-disable no-fallthrough */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {SafeAreaView, ScrollView, StatusBar, Text, View} from 'react-native';
import styles from './styles';
import {DebitCard, DebitCardAction, ProgressBar} from '@component';
import {LogoIcon} from '@image';
import {numberToLocalString} from '@common';
import {CARD_ACTIONS} from '../../components/debit-card-action/constants';
import {useFocusEffect} from '@react-navigation/native';
import {useGetPendingLimit, useSetPendingLimit} from '@hook';

export function DebitCardScreen({navigation}) {
  const [showCardNumber, setShowCardNumber] = useState(false);
  const [spendingLimit, setSpendingLimit] = useState<number>(0);

  useFocusEffect(
    React.useCallback(() => {
      useGetPendingLimit().then((limit) =>{
        setSpendingLimit(limit);
      });
    }, [setSpendingLimit]),
  );

  const showCardNumberToggle = () => {
    console.log('showCardNumberToggle');
    setShowCardNumber(!showCardNumber);
  };

  const switchAction = (type: CARD_ACTIONS) => {
    switch (type) {
      case CARD_ACTIONS.WEEKLY_SPENDING_LIMIT:
        if (spendingLimit != 0) {
          setSpendingLimit(0);
          useSetPendingLimit(0);
        } else {
          setupSpendingLimit();
        }
      default:
        break;
    }
  };

  const setupSpendingLimit = () => {
    navigation.navigate('SpendingLimit', {
      spendingLimit: spendingLimit,
    });
  };

  function SpendingLimitProgressView() {
    return (
      <View>
        <View style={styles.progressBarView}>
          <Text style={styles.progressHeaderText}>
            Debit card spending limit
          </Text>
          <View style={styles.spendingLimitationWrapView}>
            <Text style={styles.spendingCurrentText}>$345</Text>
            <View style={styles.separatorLine} />
            <Text style={styles.spendingLimitationText}>
              ${numberToLocalString(spendingLimit)}
            </Text>
          </View>
        </View>
        <ProgressBar maxValue={spendingLimit} />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      <View style={styles.headerViewWrapper}>
        <SafeAreaView style={styles.headerView}>
          <View style={styles.appLogo}>
            <LogoIcon />
          </View>
          <Text style={styles.title}>Debit Card</Text>
          <Text style={styles.availableText}>Available balance</Text>
          <View style={styles.balenceView}>
            <View style={styles.prefixBalenceView}>
              <Text style={styles.prefixBalenceText}>S$</Text>
            </View>
            <Text style={[styles.title, {paddingLeft: 10}]}>3000</Text>
          </View>
        </SafeAreaView>
      </View>
      <SafeAreaView style={styles.contentView}>
        <ScrollView
          style={styles.scrollView}
          bounces={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{marginTop: 124, paddingBottom: 124}}>
          <View style={styles.scrollContentView}>
            {DebitCard(null, showCardNumber, () => showCardNumberToggle())}
            <View style={styles.actionView}>
              {spendingLimit > 0 ? <SpendingLimitProgressView /> : null}
              {DebitCardAction(
                CARD_ACTIONS.TOP_UP,
                'Top-up account',
                'Deposit money to your account to use with card',
                null,
                null,
                null,
              )}
              {DebitCardAction(
                CARD_ACTIONS.WEEKLY_SPENDING_LIMIT,
                'Weekly spending limit',
                'You haven’t set any spending limit on card',
                () => switchAction(CARD_ACTIONS.WEEKLY_SPENDING_LIMIT),
                () => setupSpendingLimit(),
                spendingLimit > 0,
              )}
              {DebitCardAction(
                CARD_ACTIONS.FREEZE_CARD,
                'Freeze card',
                'Your debit card is currently active',
                () => switchAction(CARD_ACTIONS.FREEZE_CARD),
                null,
                null,
              )}
              {DebitCardAction(
                CARD_ACTIONS.GET_NEW_CARD,
                'Get a new card',
                'This deactivates your current debit card',
                null,
                null,
                null,
              )}
              {DebitCardAction(
                CARD_ACTIONS.DEACTIVATED_CARD,
                'Deactivated cards',
                'Your previously deactivated cards',
                null,
                null,
                null,
              )}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
