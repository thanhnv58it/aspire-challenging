/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {SafeAreaView, TouchableOpacity, Text, View} from 'react-native';
import styles from './styles';
import {BackIcon, LogoIcon, SpendingLitmitSmall} from '@image';
import {useNavigation} from '@react-navigation/native';
import {COLORS} from '@theme';
import {numberToLocalString} from '@common';
import {useSetPendingLimit} from '@hook';

export function SpendingLimitScreen({route}) {
  const navigation = useNavigation();

  const [currentLimit, setCurrentLimit] = useState(route.params.spendingLimit);

  const [activeButton, setActiveButton] = useState(currentLimit > 0);

  const backAction = () => {
    navigation.goBack();
  };

  const onPress = (limit) => {
    setCurrentLimit(limit);
    setActiveButton(true);
  };

  const savePress = () => {
    if (!activeButton) {
      return;
    }
    useSetPendingLimit(currentLimit);
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <View style={styles.headerViewWrapper}>
          <View style={styles.navigationBar}>
            <TouchableOpacity onPress={() => backAction()}>
              <BackIcon />
            </TouchableOpacity>
            <View style={{flex: 1}} />
            <LogoIcon />
          </View>
          <Text style={styles.title}>Spending limit</Text>
        </View>
      </SafeAreaView>
      <View style={styles.bottomWrapView}>
        <View style={styles.descriptionWrapView}>
          <SpendingLitmitSmall />
          <Text style={styles.description}>
            Set a weekly debit card spending limit
          </Text>
        </View>
        <View style={styles.balenceView}>
          <View style={styles.prefixBalenceView}>
            <Text style={styles.prefixBalenceText}>S$</Text>
          </View>
          <Text style={styles.selectSpendingLimitText}>
            {currentLimit > 0 ? numberToLocalString(currentLimit) : ''}
          </Text>
        </View>
        <View style={styles.separatorLine} />
        <Text style={styles.timeDescription}>
          Here weekly means the last 7 days - not the calendar week
        </Text>
        <View style={styles.groupOptionContainer}>
          <TouchableOpacity
            style={styles.optionContainer}
            onPress={() => onPress(5000)}>
            <Text style={styles.optionText}>S$ 5,000</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.optionContainer}
            onPress={() => onPress(10000)}>
            <Text style={styles.optionText}>S$ 10,000</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.optionContainer}
            onPress={() => onPress(20000)}>
            <Text style={styles.optionText}>S$ 20,000</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}} />
        <TouchableOpacity
          activeOpacity={activeButton ? 0.5 : 1}
          onPress={() => savePress()}
          style={[
            styles.saveButton,
            {backgroundColor: activeButton ? COLORS.PRIMARY : '#EEEEEE'},
          ]}>
          <View>
            <Text style={styles.saveButtonText}>Save</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}
