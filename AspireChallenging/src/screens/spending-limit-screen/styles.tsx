import {StyleSheet} from 'react-native';
import {COLORS, FONTS} from '@theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BACKGROUND,
  },
  navigationBar: {
    flexDirection: 'row',
  },
  headerViewWrapper: {
    height: 132,
    paddingHorizontal: 24,
    paddingTop: 12,
  },
  bottomWrapView: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    padding: 24,
    paddingTop: 32,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
  },
  title: {
    ...FONTS.bold,
    textAlign: 'left',
    fontSize: 24,
    color: 'white',
    paddingTop: 20,
  },
  descriptionWrapView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  description: {
    ...FONTS.medium,
    textAlign: 'left',
    fontSize: 14,
    color: COLORS.SUB_TITLE,
    paddingHorizontal: 12,
  },
  balenceView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 12,
    paddingBottom: 6,
    height: 52,
  },
  prefixBalenceText: {
    ...FONTS.bold,
    textAlign: 'center',
    fontSize: 14,
    color: 'white',
  },
  prefixBalenceView: {
    backgroundColor: COLORS.PRIMARY,
    alignContent: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    paddingHorizontal: 12,
    paddingVertical: 2,
  },
  selectSpendingLimitText: {
    ...FONTS.bold,
    textAlign: 'left',
    fontSize: 24,
    color: COLORS.SUB_TITLE,
    paddingLeft: 10,
  },
  separatorLine: {
    height: 1,
    width: '100%',
    backgroundColor: COLORS.LIGHT_GRAY,
  },
  timeDescription: {
    ...FONTS.regular,
    textAlign: 'left',
    fontSize: 13,
    color: '#22222266',
    paddingVertical: 12,
  },
  optionContainer: {
    height: 40,
    width: '31%',
    backgroundColor: '#01D16712',
    borderRadius: 4,
    justifyContent: 'center',
  },
  optionText: {
    ...FONTS.demiBold,
    textAlign: 'center',
    fontSize: 12,
    color: COLORS.PRIMARY,
    paddingHorizontal: 12,
  },
  groupOptionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  saveButton: {
    height: 56,
    margin: 24,
    backgroundColor: COLORS.PRIMARY,
    borderRadius: 30,
    justifyContent: 'center',
  },
  saveButtonText: {
    ...FONTS.demiBold,
    textAlign: 'center',
    fontSize: 16,
    color: COLORS.WHITE,
  },
});
