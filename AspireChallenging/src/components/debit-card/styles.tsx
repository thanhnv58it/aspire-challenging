import {StyleSheet} from 'react-native';
import {COLORS, FONTS} from '@theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-end',
    position: 'absolute',
    zIndex: 3,
    height: 264,
    width: '85%',
  },
  actionContainer: {
    height: 44,
    width: 150,
    backgroundColor: COLORS.WHITE,
    borderRadius: 6,
    alignItems: 'center',
  },
  touchableContainer: {
    height: 30,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  actionText: {
    ...FONTS.demiBold,
    textAlign: 'left',
    fontSize: 12,
    color: COLORS.PRIMARY,
    padding: 4,
  },
  title: {
    ...FONTS.bold,
    textAlign: 'left',
    fontSize: 22,
    color: COLORS.WHITE,
    width: '100%',
    paddingVertical: 22,
  },
  cardNumberText: {
    ...FONTS.demiBold,
    textAlign: 'left',
    fontSize: 14,
    color: COLORS.WHITE,
    paddingHorizontal: 1,
  },
  expiredText: {
    ...FONTS.demiBold,
    textAlign: 'left',
    fontSize: 13,
    color: COLORS.WHITE,
  },
  CVVText: {
    ...FONTS.demiBold,
    textAlign: 'left',
    fontSize: 24,
    color: COLORS.WHITE,
    paddingLeft: 4,
  },
  cardView: {
    height: 220,
    width: '100%',
    backgroundColor: COLORS.PRIMARY,
    borderRadius: 12,
    marginTop: -14,
    paddingHorizontal: 20,
    paddingVertical: 24,
    alignItems: 'flex-end',
  },
  dotView: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: 'white',
    marginHorizontal: 2,
  },
  dotGroup: {
    flexDirection: 'row',
    paddingRight: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardNumberWrapView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
