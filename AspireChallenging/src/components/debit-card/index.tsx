/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import {ShowCardIcon, HideCardIcon, AspireLogo, VisaLogoIcon} from '@image';

export function DebitCard(card: any, isShowCard: boolean, action: any) {
  function HiddenCardNumberGroup({numbers, isShowNumber}) {
    const array = [1, 2, 3, 4].map((index: number) => {
      return isShowNumber ? (
        <Text style={styles.cardNumberText} key={index}>
          {numbers[index - 1]}
        </Text>
      ) : (
        <View style={styles.dotView} key={index} />
      );
    });
    return <View style={styles.dotGroup}>{array}</View>;
  }

  function PrefixCardNumberGroup() {
    const array = [1, 2, 3, 4].map((index: number) => {
      const numbers = cardNumber.toString().substring(4 * index - 4, 4 * index);
      return (
        <HiddenCardNumberGroup
          numbers={numbers}
          isShowNumber={index === 4 || isShowCard}
          key={index}
        />
      );
    });
    return <View style={{flexDirection: 'row'}}>{array}</View>;
  }

  const cardNumber = 5647341124132020;
  const CVV = 456;

  return (
    <View style={styles.container}>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.touchableContainer} onPress={action}>
          {isShowCard ? <HideCardIcon /> : <ShowCardIcon />}
          <Text style={styles.actionText}>
            {isShowCard ? 'Hide card number' : 'Show card number'}
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.cardView}>
        <AspireLogo />
        <Text style={styles.title}>Mark Henry</Text>
        <View style={styles.cardNumberWrapView}>
          <PrefixCardNumberGroup />
          <View style={{flex: 1}} />
        </View>
        <View
          style={[
            styles.cardNumberWrapView,
            {paddingTop: 12, paddingBottom: 4, height: 32},
          ]}>
          <Text style={[styles.expiredText, {paddingRight: 32}]}>
            Thru: 12/20
          </Text>
          <View style={styles.cardNumberWrapView}>
            <Text style={styles.expiredText}>CVV:</Text>
            <Text
              style={[
                styles.CVVText,
                {fontSize: isShowCard ? 13 : 24, height: isShowCard ? 18 : 24},
              ]}>
              {isShowCard ? CVV : '***'}
            </Text>
          </View>
          <View style={{flex: 1}} />
        </View>
        <VisaLogoIcon />
      </View>
    </View>
  );
}
