import {StyleSheet} from 'react-native';
import {COLORS} from '@theme';

export default StyleSheet.create({
  container: {
    height: 15,
    width: '100%',
    borderRadius: 7.5,
    backgroundColor: '#01D16720',
    marginBottom: 16,
  },
  parallelogram: {
    height: 15,
    flexDirection: 'row',
  },
  parallelogramInner: {
    position: 'absolute',
    left: 0,
    top: 0,
    backgroundColor: COLORS.PRIMARY,
    height: 15,
    borderTopLeftRadius: 7.5,
    borderBottomLeftRadius: 7.5,
  },

  parallelogramLeft: {
    top: 0,
    position: 'absolute',
  },
  triangleCorner: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 8,
    borderTopWidth: 15,
    borderRightColor: 'transparent',
    borderTopColor: COLORS.PRIMARY,
  },
});
