import React from 'react';
import {View} from 'react-native';
import styles from './styles';

const TriangleCorner = ({style}) => {
  return <View style={[styles.triangleCorner, style]} />;
};

const Parallelogram = (maxValue) => {
  const percentage = (345 / maxValue) * 100;
  const width = percentage.toString() + '%';

  return (
    <View style={styles.parallelogram}>
      <View style={[styles.parallelogramInner, {width: width}]} />
      <TriangleCorner style={[styles.parallelogramLeft, {left: width}]} />
    </View>
  );
};

export function ProgressBar({maxValue}) {
  return <View style={styles.container}>{Parallelogram(maxValue)}</View>;
}
