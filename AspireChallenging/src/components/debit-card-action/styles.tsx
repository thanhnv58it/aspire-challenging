import {StyleSheet} from 'react-native';
import {COLORS, FONTS} from '@theme';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    paddingVertical: 16,
  },
  textContainer: {
    paddingHorizontal: 12,
    flex: 1,
  },
  title: {
    ...FONTS.medium,
    textAlign: 'left',
    fontSize: 14,
    color: COLORS.TITLE,
  },
  subText: {
    ...FONTS.regular,
    textAlign: 'left',
    fontSize: 13,
    color: COLORS.SUB_TITLE,
    opacity: 0.4,
  },
  switcher: {
    transform: [{scaleX: 0.7}, {scaleY: 0.7}],
  },
});
