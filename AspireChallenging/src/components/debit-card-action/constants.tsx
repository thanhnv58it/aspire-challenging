export enum CARD_ACTIONS {
  TOP_UP,
  WEEKLY_SPENDING_LIMIT,
  FREEZE_CARD,
  GET_NEW_CARD,
  DEACTIVATED_CARD,
}
