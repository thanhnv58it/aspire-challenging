/* eslint-disable no-undef */
import React from 'react';
import {Switch, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import {
  InsightLogo,
  SpendingLitmitIcon,
  NewCardIcon,
  FreezeCardIcon,
  DeactivatedCardIcon,
} from '@image';
import {CARD_ACTIONS} from './constants';

export function DebitCardAction(
  type: CARD_ACTIONS,
  title: string,
  description: string,
  toggleSwitch: any,
  tapAction: any,
  swicherValue: boolean | null,
) {
  const Icon = () => {
    switch (type) {
      case CARD_ACTIONS.TOP_UP:
        return <InsightLogo />;
      case CARD_ACTIONS.FREEZE_CARD:
        return <FreezeCardIcon />;
      case CARD_ACTIONS.GET_NEW_CARD:
        return <NewCardIcon />;
      case CARD_ACTIONS.DEACTIVATED_CARD:
        return <DeactivatedCardIcon />;

      default:
        return <SpendingLitmitIcon />;
    }
  };

  return (
    <TouchableOpacity style={styles.container} onPress={tapAction}>
      <Icon />
      <View style={styles.textContainer}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subText}>{description}</Text>
      </View>
      {toggleSwitch == null ? null : (
        <Switch
          trackColor={{false: '#BDBDBD20'}}
          style={styles.switcher}
          onValueChange={toggleSwitch}
          value={swicherValue || false}
        />
      )}
    </TouchableOpacity>
  );
}
