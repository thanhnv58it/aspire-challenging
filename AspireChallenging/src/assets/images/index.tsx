import AccountIcon from './account.svg';
import HomeIcon from './home.svg';
import PayIcon from './pay.svg';
import PaymenstIcon from './payments.svg';
import CreditIcon from './credit.svg';
import HideCardIcon from './hide_card.svg';
import ShowCardIcon from './show_card_number.svg';
import LogoIcon from './logo.svg';
import VisaLogoIcon from './visa_logo.svg';
import AspireLogo from './aspire_logo.svg';
import InsightLogo from './insight.svg';
import SpendingLitmitIcon from './spending_limit.svg';
import NewCardIcon from './get_new_card.svg';
import FreezeCardIcon from './transfer_3.svg';
import DeactivatedCardIcon from './transfer.svg'
import BackIcon from './back.svg'
import SpendingLitmitSmall from './speding_limit_small'

export {
  AccountIcon,
  HomeIcon,
  PayIcon,
  PaymenstIcon,
  CreditIcon,
  HideCardIcon,
  ShowCardIcon,
  LogoIcon,
  VisaLogoIcon,
  AspireLogo,
  InsightLogo,
  SpendingLitmitIcon,
  NewCardIcon,
  FreezeCardIcon,
  DeactivatedCardIcon,
  BackIcon,
  SpendingLitmitSmall,
};
