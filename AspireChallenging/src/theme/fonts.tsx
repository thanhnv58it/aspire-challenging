import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  regular: {
    fontFamily: 'AvenirNext-Regular',
    color: 'black',
  },
  medium: {
    fontFamily: 'AvenirNext-Medium',
    color: 'black',
  },
  bold: {
    fontFamily: 'AvenirNext-Bold',
    color: 'black',
  },
  demiBold: {
    fontFamily: 'AvenirNext-DemiBold',
    color: 'black',
  },
});
