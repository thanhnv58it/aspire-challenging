import AsyncStorage from '@react-native-community/async-storage';

export async function useGetPendingLimit(): Promise<number> {
  const limit = await AsyncStorage.getItem('SpendingLimit');
  const limitValue = Number(limit);
  return limitValue;
}

export function useSetPendingLimit(value: number) {
  AsyncStorage.setItem('SpendingLimit', value.toString());
}
